# cloc.py - count lines of code for mercurial
#
# Copyright 2012 Jin Hui <pxjinhui@gmail.com>
#
# Includes code from:
#   -CLOC v1.55
#   http://cloc.sourceforge.net/
#   by Al Danial

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.

from mercurial.i18n import _
from mercurial import cmdutil, scmutil, commands
from mercurial.util import binary
import os
import re

def cloc(ui, repo, *pats, **opts):
    """command to display statistics about files and lines of code"""

    def pad(s, l):
        return (s + " " * l)[:l]

    rate = countrate(ui, repo, *pats, **opts).items()
    if not rate:
        return

    maxname = max(len(k) for k, v in rate)
    ttywidth = ui.termwidth()
    fields = 5
    width = (ttywidth - maxname) / fields if maxname < ttywidth/2 else (ttywidth*2 - maxname) / fields
    nchar = '-' * ttywidth + '\n'

    def format(name, count):
        pstr = pad(name, maxname)
        for c in count:
            pstr += ('%%%ds' % width) % c
        pstr += '\n'
        return pstr

    SUM = []
    if opts['file']:
        ui.write(format('filename', ['language', 'blank', 'comment', 'code', 'total']))
    else:
        ui.write(format('language', ['files', 'blank', 'comment', 'code', 'total']))
    ui.write(nchar)
    for name, count in rate:
        ui.write(format(name, count))
        if not SUM:
            SUM = count[:]
            continue
        for i in range(len(count)):
            SUM[i] += count[i]
    for i in range(len(SUM)):
        if not isinstance(SUM[i], int):
            SUM[i] = ''
    ui.write(nchar)
    ui.write(format('SUM:', SUM))

def countrate(ui, repo, *pats, **opts):
    state = {'count': 0}
    rate = {}
    m = scmutil.match(repo[None], pats, opts)

    def prep(ctx, fns):
        for path in ctx.manifest():
            if binary(ctx[path].data()):
                continue
            ignore = False
            if opts.get('ignore'):
                for pattern in opts['ignore']:
                    if re.search(pattern, path):
                        ignore = True
                        break
            if ignore:
                continue

            filename = os.path.split(path)[-1]
            language = LANGUAGE_BY_FILE.get(filename)
            lines = ctx[path].data().splitlines(True)

            if not language:
                basename = os.path.splitext(path)[0]
                ext = os.path.splitext(path)[-1]
                language = LANGUAGE_BY_FILE.get(basename) or LANGUAGE_BY_EXTENSION.get(ext)

            if not language:
                language = detection_shebang(lines)

            if language:
                total_lines = len(lines)
                lines = rm_blanks(lines, language)
                blank_lines = total_lines - len(lines)
                if language == 'Haskell':
                    lines = remove_haskell_comments(lines, ext)
                else:
                    if language == 'PHP/Pascal':
                        if really_is_php(lines):
                            language = 'PHP'
                        if really_is_incpascal(lines):
                            language = 'Pascal'
                    if language == 'MATLAB/Objective C/MUMPS':
                        language = matlab_or_objective_C(lines)
                    lines = rm_comments(lines, language)
                comments_lines = total_lines - blank_lines - len(lines)
                code_lines = len(lines)
                if opts.get('file'):
                    rate[path] = [language, blank_lines, comments_lines, code_lines, total_lines]
                else:
                    temp = rate.get(language, [0,0,0,0,0])
                    rate[language] = [temp[0] + 1, temp[1] + blank_lines, temp[2] + comments_lines, temp[3] + code_lines, temp[4] + total_lines]

            state['count'] += 1
            ui.progress(_('analyzing'), state['count'], total=len(ctx.manifest()))

    if not opts.get('rev'):
        opts['rev'] = ['tip']
    opts['rev'] = opts['rev'][0:1]

    for ctx in cmdutil.walkchangerevs(repo, m, opts, prep):
        continue

    ui.progress(_('analyzing'), None)

    return rate

def rm_blanks(lines, language):
    if language == 'COBOL':
        return remove_cobol_blanks(lines)
    else:
        return remove_matches(lines, '^\s*$')

def rm_comments(lines, language):
    routines = FILTERS_BY_LANGUAGE.get(language)

    if not routines:
        return lines

    if not lines:
        return lines

    original_lines = lines[:]

    for call_string in routines:
        subroutine = call_string[0]
        if len(call_string) == 1:
            lines = subroutine(lines)
        elif len(call_string) == 2:
            lines = subroutine(lines, call_string[1])
        elif len(call_string) == 3:
            lines = subroutine(lines, call_string[1], call_string[2])

    lines = remove_matches(lines, '^\s*$')

    # Exception for scripting languages:  treat the first #! line as code.
    # Will need to add it back in if it was removed earlier.
    if language in SCRIPT_LANGUAGE and re.search('^#!',original_lines[0]) and ( len(lines) == 0 or lines[0] != original_lines[0]):
        lines.insert(0, original_lines[0])

    return lines

def remove_matches(lines, pattern):
    save_lines = []
    for line in lines:
        if re.search(pattern, line, re.I):
            continue
        save_lines.append(line)
    return save_lines

def remove_cobol_blanks(lines):
    free_format = False
    save_lines = []

    for line in lines:
        if re.search('^\s*$', line):
            continue
        rline = line.replace('\t','    ')
        if re.search('^......\$.*SET.*SOURCEFORMAT.*FREE', rline, re.I):
            free_format = True
        if free_format:
            save_lines.append(line)
        else:
            # Greg Toth:
            #  (1) Treat lines with any alphanum in cols 1-6 and 
            #      blanks in cols 7 through 71 as blank line, and
            #  (2) Treat lines with any alphanum in cols 1-6 and 
            #      slash (/) in col 7 as blank line (this is a 
            #      page eject directive). 
            if not ( re.search('^\d{6}\s*$', line) or re.search('^.{6}\s{66}', rline) or re.search('^......\/', rline) ):
                save_lines.append(line)

    return save_lines

def remove_cobol_comments(lines):
    free_format = False
    save_lines = []

    for line in lines:
        if re.search('^......\$.*SET.*SOURCEFORMAT.*FREE', line, re.I):
            free_format = True
        if free_format:
            if not re.search('^\s*\*', line):
                save_lines.append(line)
        else:
            if not (re.search('......\*', line) or re.search('^\*', line)):
                save_lines.append(line)

    return save_lines

def call_regexp_common(lines, language):
    pattern = RE_COMMENT.get(language)
    if not pattern:
        return lines

    all_lines = ''.join(lines)
    if re.search(pattern, all_lines):
        all_lines = re.sub(pattern, '', all_lines)

    return all_lines.splitlines(True)

def docstring_to_C(lines):
    '''Converts Python docstrings to C comments.'''

    in_docstring = False
    for line in lines:
        while re.search('"""', line):
            if not in_docstring:
                line = re.sub('"""', '/\*', line)
                in_docstring = True
            else:
                line = re.sub('"""', '\*/', line)
                in_docstring = False
    return lines

def remove_f77_comments(lines):
    save_lines = []
    for line in lines:
        if re.search('^[*cC]', line):
            continue
        if re.search('^\s*!', line):
            continue
        save_lines.append(line)
    return save_lines

def remove_f90_comments(lines):
    save_lines = []
    for line in lines:
        # a comment is              m/^\s*!/
        # an empty line is          m/^\s*$/
        # a HPF statement is        m/^\s*!hpf\$/i
        # an Open MP statement is   m/^\s*!omp\$/i
        if ( not re.search('^(\s*!|\s*$)', line) ) or re.search('^\s*!(hpf|omp)\$', line):
            save_lines.append(line)
    return save_lines

def remove_above(lines, maker):
    '''Make two passes through the code:
    1. check if the marker exists
    2. remove anything above the marker if it exists,
       do nothing if the marker does not exist'''

    # Pass 1
    found_marker = 0
    for line_number in range(len(lines)):
        if re.search(maker, lines[line_number]):
            found_marker = line_number
            break

    # Pass 2 only if needed
    if found_marker:
        n = 0
        save_lines = []
        for line in lines:
            if n >= found_marker:
                save_lines.append(line)
            n += 1
        return save_lines
    return lines

def smarty_to_C(lines):
    '''Converts Smarty comments to C comments.'''

    for line in lines:
        line = re.sub('{\*', '/*', line)
        line = re.sub('\*}', '*/', line)
    return lines

def remove_below(lines, marker):
    save_lines = []
    for line in lines:
        if re.search(marker, line):
            break
        save_lines.append(line)
    return save_lines

def remove_below_above(lines, marker_below, marker_above):
    '''delete lines delimited by start and end line markers such as Perl POD documentation'''

    save_lines = []
    between = False
    for line in lines:
        if not between and re.search(marker_below, line):
            between = True
            continue
        if between and re.search(marker_above, line):
            between = False
            continue
        if between:
            continue
        save_lines.append(line)
    return save_lines

def remove_jcl_comments(lines):
    save_lines = []
    for line in lines:
        if re.search('^\s*$', line):
            continue
        if re.search('^\s*//\*', line):
            continue
        if re.search('^\s*//\s*$', line):
            break
        save_lines.append(line)
    return save_lines

def remove_jsp_comments(lines):
    '''JSP comment is   <%--  body of comment   --%>'''
    save_lines = []
    in_comment = False
    for line in lines:
        if re.search('^\s*$', line):
            continue
        line = re.sub('<\%\-\-.*?\-\-\%>', '', line) # strip one-line comments
        if re.search('^\s*$', line):
            continue
        if in_comment:
            if re.search('\-\-\%>', line):
                line = re.sub('^.*?\-\-\%>', '', line)
                in_comment = False
        if re.search('^\s*$', line):
            continue
        if re.search('^(.*?)<\%\-\-', line):
            in_comment = True
        if in_comment:
            continue
        save_lines.append(line)
    return save_lines

def remove_haskell_comments(lines, ext):
    '''Bulk of code taken from SLOCCount's haskell_count script.
    Strips out {- .. -} and -- comments and counts the rest.
    Pragmas, {-#...}, are counted as SLOC.
    BUG: Doesn't handle strings with embedded block comment markers gracefully.
         In practice, that shouldn't be a problem.'''

    save_lines = []
    in_comment = in_litblock = False
    literate = 0
    if ext == '.lhs':
        literate = 1
    if literate:
        literate = determine_lit_type(lines)

    for line in lines:
        if literate == 1:
            line = re.sub('^>', '', line)
            if not line:
                line = re.sub('.*', '', line)
        elif literate == 2:
            if in_litblock:
                if re.search('^\\end{code}', line):
                    line = re.sub('.*', '', line)
                    in_litblock = False
            else:
                if re.search('^\\begin{code}', line):
                    line = re.sub('.*', '', line)
                    in_litblock = True
        if in_comment:
            if re.search('\-\}', line):
                line = re.sub('^.*?\-\}', '', line)
                in_comment = False
        if not in_comment:
            line = re.sub('--.*', '', line)
            line = re.sub('{-[^#].*?-}', '', line)
            if re.search('{-', line) and not re.search('{-#', line):
                line = re.sub('{-.*', '', line)
                in_comment = True
        if re.search('\S', line):
            save_lines.append(line)

    return remove_matches(save_lines, '^\s*$')

def determine_lit_type(lines):
    text = ''.join(lines)
    if re.search('^\\begin{code}', text):
        return 2
    if re.search('^>\s', text):
        return 1
    return 0

def really_is_incpascal(lines):
    is_pascal = False
    found_begin = False
    for line in lines:
        line = re.sub('\{.*?\}', '', line) # Ignore {...} comments on this line; imperfect, but effective.
        line = re.sub('\(\*.*?\*\)', '', line) # Ignore (*...*) comments on this line; imperfect, but effective.
        if re.search('\bprogram\s+[A-Za-z]', line, re.I):
            is_pascal = True
        if re.search('\bunit\s+[A-Za-z]', line, re.I):
            is_pascal = True
        if re.search('\bmodule\s+[A-Za-z]', line, re.I):
            is_pascal = True
        if re.search('\bprocedure\b', line, re.I):
            is_pascal = True
        if re.search('\bfunction\b', line, re.I):
            is_pascal = True
        if re.search('^\s*interface\s+', line, re.I):
            is_pascal = True
        if re.search('^\s*implementation\s+', line, re.I):
            is_pascal = True
        if re.search('\bconstant\s+', line, re.I):
            is_pascal = True
        if re.search('\bbegin\b', line, re.I):
            found_begin = True
        if re.search('end\.\s*$', line, re.I) and found_begin:
            is_pascal = True
        if is_pascal:
            break
    return is_pascal

def really_is_php(lines):
    is_php = False
    normal_surround = 0 # <?; bit 0 = <?, bit 1 = ?>
    script_surround = 0 # <script..>; bit 0 = <script language="php">
    asp_surround = 0 # <%; bit 0 = <%, bit 1 = %>
    for line in lines:
        if re.search('\<\?', line):
            normal_surround |= 1
        if re.search('\?\>', line) and (normal_surround & 1):
            normal_surround |= 2
        if re.search('\<script.*language="?php"?', line, re.I):
            script_surround |= 1
        if re.search('\<\/script\>', line, re.I) and (script_surround & 1):
            script_surround |= 2
        if re.search('\<\%', line):
            asp_surround |= 1
        if re.search('\%\>', line) and (asp_surround & 1):
            asp_surround |= 2
    if normal_surround == 3 or script_surround == 3 or asp_surround == 3:
        is_php = True
    return is_php

def matlab_or_objective_C(lines):
    '''Decide if code is MATLAB, Objective C, or MUMPS

    matlab markers:
      first line starts with "function"
      some lines start with "%"
      high marks for lines that start with [

    Objective C markers:
      must have at least two brace characters, { }
      has /* ... */ style comments
      some lines start with @
      some lines start with #include

    MUMPS:
      has ; comment markers
      do not match:  \w+\s*=\s*\w
      lines begin with   \s*\.?\w+\s+\w
      high marks for lines that start with \s*K\s+ or \s*Kill\s+'''

    matlab_points = 0
    objective_C_points = 0
    mumps_points = 0
    has_braces = 0

    for i, line in enumerate(lines):
        if re.search('[{}]', line):
            has_braces += 1
        if i == 0 and re.search('^[A-Z]', line):
            mumps_points += 1
        if re.search('^\s*/\*', line): #   /*
            objective_C_points += 1
            matlab_points -= 1
        elif re.search('\w+\s*=\s*\[', line): # matrix assignment, very matlab
            matlab_points += 5
        elif re.search('^\s*\w+\s*=\s*', line): # definitely not MUMPS
            mumps_points -= 1
        elif re.search('^\s*\.?[A-Za-z_]\s+[A-Za-z_]', line):
            mumps_points += 1
        elif re.search('^\s*;', line):
            mumps_points += 1
        elif re.search('^\s*#(include|import)', line):
            # Objective C without a doubt
            objective_C_points = 1
            matlab_points = 0
            break
        elif re.search('^\s*@(interface|implementation|protocol|public|protected|private|end)\s', line):
            # Objective C without a doubt
            objective_C_points = 1
            matlab_points = 0
            break
        elif re.search('^\s*\[', line): #   line starts with [  -- very matlab
            matlab_points += 5
        elif re.search('^\sK(ill)?\s+', line):
            mumps_points += 5
        elif re.search('^\s*function', line):
            objective_C_points -= 1
            matlab_points += 1
        elif re.search('^\s*%', line): #   %
            objective_C_points -= 1
            matlab_points += 1
            mumps_points += 1

    if has_braces < 2:
        objective_C_points = -9.9e20
    if matlab_points > objective_C_points and matlab_points > mumps_points:
        return 'MATLAB'
    elif mumps_points > objective_C_points and mumps_points > matlab_points:
        return 'MUMPS'
    else:
        return 'Objective C'

def die(lines):
    '''never called'''
    return lines

def get_shebang(lines):
    line = lines[0]
    if len(line) <= 2:
        return None
    if line[0:2] == '#!':
        return line
    return None

def detection_shebang(lines):
    if not lines:
        return lines

    shebang = get_shebang(lines)
    if shebang is None:
        return None
    for language in SCRIPT_LANGUAGE:
        for pattern in SCRIPT_LANGUAGE[language]:
            rgx = re.compile(r"^#!((?:/[^\s]+/env(?:\.[a-z]+)? |/[^\s]+/|[A-Z]:[^\s]*\\|[A-Z]:[^\s]*\\env\.[a-z]+ ?)?%s[\d.-_]*(?:\.[0-9a-z-_.])*)\b" % pattern, re.IGNORECASE)
            if rgx.search(shebang):
                return language
    return None

LANGUAGE_BY_EXTENSION = {
    '.abap'        : 'ABAP'                  ,
    '.ac'          : 'm4'                    ,
    '.ada'         : 'Ada'                   ,
    '.adb'         : 'Ada'                   ,
    '.ads'         : 'Ada'                   ,
    '.adso'        : 'ADSO/IDSM'             ,
    '.am'          : 'make'                  ,
    '.ample'       : 'AMPLE'                 ,
    '.as'          : 'ActionScript'          ,
    '.dofile'      : 'AMPLE'                 ,
    '.startup'     : 'AMPLE'                 ,
    '.asa'         : 'ASP'                   ,
    '.asax'        : 'ASP.Net'               ,
    '.ascx'        : 'ASP.Net'               ,
    '.asm'         : 'Assembly'              ,
    '.asmx'        : 'ASP.Net'               ,
    '.asp'         : 'ASP'                   ,
    '.aspx'        : 'ASP.Net'               ,
    '.master'      : 'ASP.Net'               ,
    '.sitemap'     : 'ASP.Net'               ,
    '.awk'         : 'awk'                   ,
    '.bash'        : 'Bourne Again Shell'    ,
    '.bas'         : 'Visual Basic'          ,
    '.bat'         : 'DOS Batch'             ,
    '.BAT'         : 'DOS Batch'             ,
    '.cbl'         : 'COBOL'                 ,
    '.CBL'         : 'COBOL'                 ,
    '.c'           : 'C'                     ,
    '.C'           : 'C++'                   ,
    '.cc'          : 'C++'                   ,
    '.ccs'         : 'CCS'                   ,
    '.cfm'         : 'ColdFusion'            ,
    '.cl'          : 'Lisp'                  ,
    '.cls'         : 'Visual Basic'          ,
    '.CMakeLists.txt': 'CMake'              ,
    '.cob'         : 'COBOL'                 ,
    '.COB'         : 'COBOL'                 ,
    '.config'      : 'ASP.Net'               ,
    '.cpp'         : 'C++'                   ,
    '.cs'          : 'C#'                    ,
    '.csh'         : 'C Shell'               ,
    '.css'         : "CSS"                   ,
    '.cu'          : "NVIDIA CUDA"           ,
    '.cxx'         : 'C++'                   ,
    '.d'           : 'D'                     ,
    '.dart'        : 'Dart'                  ,
    '.def'         : 'Teamcenter def'        ,
    '.dmap'        : 'NASTRAN DMAP'          ,
    '.dpr'         : 'Pascal'                ,
    '.dtd'         : 'DTD'                   ,
    '.ec'          : 'C'                     ,
    '.el'          : 'Lisp'                  ,
    '.erl'         : 'Erlang'                ,
    '.exp'         : 'Expect'                ,
    '.f77'         : 'Fortran 77'            ,
    '.F77'         : 'Fortran 77'            ,
    '.f90'         : 'Fortran 90'            ,
    '.F90'         : 'Fortran 90'            ,
    '.f95'         : 'Fortran 95'            ,
    '.F95'         : 'Fortran 95'            ,
    '.f'           : 'Fortran 77'            ,
    '.F'           : 'Fortran 77'            ,
    '.fmt'         : 'Oracle Forms'          ,
    '.focexec'     : 'Focus'                 ,
    '.frm'         : 'Visual Basic'          ,
    '.gnumakefile' : 'make'                  ,
    '.Gnumakefile' : 'make'                  ,
    '.go'          : 'Go'                    ,
    '.groovy'      : 'Groovy'                ,
    '.h'           : 'C/C++ Header'          ,
    '.H'           : 'C/C++ Header'          ,
    '.hh'          : 'C/C++ Header'          ,
    '.hpp'         : 'C/C++ Header'          ,
    '.hrl'         : 'Erlang'                ,
    '.hs'          : 'Haskell'               ,
    '.htm'         : 'HTML'                  ,
    '.html'        : 'HTML'                  ,
    '.i3'          : 'Modula3'               ,
    '.idl'         : 'IDL'                   ,
    '.pro'         : 'IDL'                   ,
    '.ig'          : 'Modula3'               ,
    '.il'          : 'SKILL'                 ,
    '.ils'         : 'SKILL++'               ,
    '.inc'         : 'PHP/Pascal'            , # might be PHP or Pascal
    '.itk'         : 'Tcl/Tk'                ,
    '.java'        : 'Java'                  ,
    '.jcl'         : 'JCL'                   , # IBM Job Control Lang.
    '.jl'          : 'Lisp'                  ,
    '.js'          : 'Javascript'            ,
    '.jsp'         : 'JSP'                   , # Java server pages
    '.ksc'         : 'Kermit'                ,
    '.ksh'         : 'Korn Shell'            ,
    '.lhs'         : 'Haskell'               ,
    '.l'           : 'lex'                   ,
    '.lsp'         : 'Lisp'                  ,
    '.lisp'        : 'Lisp'                  ,
    '.lua'         : 'Lua'                   ,
    '.m3'          : 'Modula3'               ,
    '.m4'          : 'm4'                    ,
    '.makefile'    : 'make'                  ,
    '.Makefile'    : 'make'                  ,
    '.met'         : 'Teamcenter met'        ,
    '.mg'          : 'Modula3'               ,
#   '.mli'         : 'ML'                    , # ML not implemented
#   '.ml'          : 'ML'                    ,
    '.ml'          : 'Ocaml'                 ,
    '.m'           : 'MATLAB/Objective C/MUMPS' ,
    '.mm'          : 'Objective C++'         ,
    '.md'          : 'Markdown'              ,
    '.markdown'    : 'Markdown'              ,
    '.mdown'       : 'Markdown'              ,
    '.mkd'         : 'Markdown'              ,
    '.mkdn'        : 'Markdown'              ,
    '.mdwn'        : 'Markdown'              ,
    '.wdproj'      : 'MSBuild scripts'       ,
    '.csproj'      : 'MSBuild scripts'       ,
    '.mps'         : 'MUMPS'                 ,
    '.mth'         : 'Teamcenter mth'        ,
    '.oscript'     : 'LiveLink OScript'      ,
    '.pad'         : 'Ada'                   , # Oracle Ada preprocessor
    '.pas'         : 'Pascal'                ,
    '.pcc'         : 'C++'                   , # Oracle C++ preprocessor
    '.perl'        : 'Perl'                  ,
    '.pfo'         : 'Fortran 77'            ,
    '.pgc'         : 'C'                     , # Postgres embedded C/C++
    '.php3'        : 'PHP'                   ,
    '.php4'        : 'PHP'                   ,
    '.php5'        : 'PHP'                   ,
    '.php'         : 'PHP'                   ,
    '.plh'         : 'Perl'                  ,
    '.pl'          : 'Perl'                  ,
    '.PL'          : 'Perl'                  ,
    '.plx'         : 'Perl'                  ,
    '.pm'          : 'Perl'                  ,
    '.p'           : 'Pascal'                ,
    '.pp'          : 'Pascal'                ,
    '.psql'        : 'SQL'                   ,
    '.py'          : 'Python'                ,
    '.pyx'         : 'Cython'                ,
    '.rb'          : 'Ruby'                  ,
#   '.resx'        : 'ASP.Net'               ,
    '.rex'         : 'Oracle Reports'        ,
    '.rexx'        : 'Rexx'                  ,
    '.rhtml'       : 'Ruby HTML'             ,
    '.rst'         : 'reStructuredText'      ,
    '.s'           : 'Assembly'              ,
    '.S'           : 'Assembly'              ,
    '.scala'       : 'Scala'                 ,
    '.sbl'         : 'Softbridge Basic'      ,
    '.SBL'         : 'Softbridge Basic'      ,
    '.sc'          : 'Lisp'                  ,
    '.scm'         : 'Lisp'                  ,
    '.sed'         : 'sed'                   ,
    '.ses'         : 'Patran Command Language'   ,
    '.pcl'         : 'Patran Command Language'   ,
    '.sh'          : 'Bourne Shell'          ,
    '.smarty'      : 'Smarty'                ,
    '.sql'         : 'SQL'                   ,
    '.SQL'         : 'SQL'                   ,
    '.sproc.sql'   : 'SQL Stored Procedure'  ,
    '.spoc.sql'    : 'SQL Stored Procedure'  ,
    '.spc.sql'     : 'SQL Stored Procedure'  ,
    '.udf.sql'     : 'SQL Stored Procedure'  ,
    '.data.sql'    : 'SQL Data'              ,
    '.sv'          : 'System Verilog'        ,
    '.svh'         : 'System Verilog'        ,
    '.tcl'         : 'Tcl/Tk'                ,
    '.tcsh'        : 'C Shell'               ,
    '.tk'          : 'Tcl/Tk'                ,
    '.tpl'         : 'Smarty'                ,
    '.v'           : 'Verilog'               ,
    '.vhd'         : 'VHDL'                  ,
    '.VHD'         : 'VHDL'                  ,
    '.vhdl'        : 'VHDL'                  ,
    '.VHDL'        : 'VHDL'                  ,
    '.vba'         : 'Visual Basic'          ,
    '.VBA'         : 'Visual Basic'          ,
#   '.vbp'         : 'Visual Basic'          , # .vbp - autogenerated
    '.vb'          : 'Visual Basic'          ,
    '.VB'          : 'Visual Basic'          ,
#   '.vbw'         : 'Visual Basic'          , # .vbw - autogenerated
    '.vbs'         : 'Visual Basic'          ,
    '.VBS'         : 'Visual Basic'          ,
    '.webinfo'     : 'ASP.Net'               ,
    '.xml'         : 'XML'                   ,
    '.XML'         : 'XML'                   ,
    '.mxml'        : 'MXML'                  ,
    '.build'       : 'NAnt scripts'          ,
    '.vim'         : 'vim script'            ,
    '.xaml'        : 'XAML'                  ,
    '.xsd'         : 'XSD'                   ,
    '.XSD'         : 'XSD'                   ,
    '.xslt'        : 'XSLT'                  ,
    '.XSLT'        : 'XSLT'                  ,
    '.xsl'         : 'XSLT'                  ,
    '.XSL'         : 'XSLT'                  ,
    '.y'           : 'yacc'                  ,
    '.yaml'        : 'YAML'                  ,
    '.yml'         : 'YAML'                  ,
}

SCRIPT_LANGUAGE = {
        'awk' : [ 'awk' ],
        'bc' : [ 'bc' ],
        'Bourne Again Shell' : [ 'bash' ],
        'Bourne Shell' : [ 'sh' ],
        'C Shell' : [ 'csh' ],
        'D' : [ 'r?dmd' ],
        'IDL' : [ 'idl' ],
        'Kermit' : [ '[kw]ermit' ],
        'Korn Shell' : [ 'ksh' ],
        'Lua' : [ 'lua' ],
        'make' : [ 'make' ],
        'Octave' : [ 'octave' ],
        'Python' : [
                '(?:xt|un|i)?pythonw?',
                '(?:py|i|moz|tiny|sni)pyw?(?:-c)?',
                'epdw?',
                '[jtm]ythonw?',
        ],
        'Cython' : [ 'cythonw?' ],
        'Perl' : [ 'w?perl' ],
        'Ruby' : [
                '[ji]?[ie]r[wbi]{0,2}?(?:_swing)?',
                '[ej]?ruby[wc]?',
                'rake'
        ],
        'Javascript' : [
                '(?:node|npm)',
                '(?:narwhal|tusk)',
                '(?:jsdb|ringo|gluew?)',
                '(?:rhino|js)'
        ],
        'PHP' : [
                '(?:i?php(?:-cgi|-cli|-win)?|pharc?)'
        ],
        'sed' : [ 'sed' ],
        'Tcl/Tk' : [ '(?:tcl(?:sh)|wish)' ]
}

LANGUAGE_BY_FILE = {
    'Makefile'       : 'make'               ,
    'makefile'       : 'make'               ,
    'gnumakefile'    : 'make'               ,
    'Gnumakefile'    : 'make'               ,
    'CMakeLists.txt' : 'CMake'              ,
}

FILTERS_BY_LANGUAGE = {
    'ABAP'               : [   [ remove_matches      , '^\*'    ], ],
    'ActionScript'       : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],

    'ASP'                : [   [ remove_matches      , '^\s*\47'], ],  # \47 = '
    'ASP.Net'            : [   [ call_regexp_common  , 'C'      ], ],
    'Ada'                : [   [ remove_matches      , '^\s*--' ], ],
    'ADSO/IDSM'          : [   [ remove_matches      , '^\s*\*[\+\!]' ], ],
    'AMPLE'              : [   [ remove_matches      , '^\s*//' ], ],
    'Assembly'           : [
                                [ remove_matches      , '^\s*//' ],
                                [ remove_matches      , '^\s*;'  ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'awk'                : [   [ remove_matches      , '^\s*#'  ], ],
    'bc'                 : [   [ remove_matches      , '^\s*#'  ], ],
    'C'                  : [
                                [ remove_matches      , '^\s*//' ], # C99
                                [ call_regexp_common  , 'C'      ],
                            ],
    'C++'                : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'C/C++ Header'       : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'CMake'              : [   [ remove_matches      , '^\s*#'  ], ],
    'Cython'             : [
                                [ remove_matches      , '^\s*#'  ],
                                [ docstring_to_C                 ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'C#'                 : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'CCS'                : [   [ call_regexp_common  , 'C'      ], ],
    'CSS'                : [   [ call_regexp_common  , 'C'      ], ],
    'COBOL'              : [   [ remove_cobol_comments,         ], ],
    'ColdFusion'         : [   [ call_regexp_common  , 'HTML'   ], ],
    'Crystal Reports'    : [   [ remove_matches      , '^\s*//' ], ],
    'D'                  : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Dart'               : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'NASTRAN DMAP'       : [   [ remove_matches      , '^\s*\$' ], ],
    'DOS Batch'          : [   [ remove_matches      , '^\s*rem', ], ],
    'DTD'                : [   [ call_regexp_common  , 'HTML'   ], ],
    'Erlang'             : [   [ remove_matches      , '^\s*%'  ], ],
    'Expect'             : [   [ remove_matches      , '^\s*#'  ], ],
    'Focus'              : [   [ remove_matches      , '^\s*\-\*'  ], ],
    'Fortran 77'         : [   [ remove_f77_comments ,          ], ],
    'Fortran 90'         : [
                                [ remove_f77_comments ,          ],
                                [ remove_f90_comments ,          ],
                            ],
    'Fortran 95'         : [
                                [ remove_f77_comments ,          ],
                                [ remove_f90_comments ,          ],
                            ],
    'Go'                 : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Groovy'             : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'HTML'               : [   [ call_regexp_common  , 'HTML'   ], ],
    'IDL'                : [   [ remove_matches      , '^\s*;'  ], ],
    'JSP'                : [   [ call_regexp_common  , 'HTML'   ],
                                [ remove_jsp_comments ,          ],
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Java'               : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Javascript'         : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'JCL'                : [   [ remove_jcl_comments ,          ], ],
    'Lisp'               : [   [ remove_matches      , '^\s*;'  ], ],
    'LiveLink OScript'   : [   [ remove_matches      , '^\s*//' ], ],
    'Lua'                : [   [ remove_matches      , '^\s*\-\-' ], ],
    'make'               : [   [ remove_matches      , '^\s*#'  ], ],
    'MATLAB'             : [   [ remove_matches      , '^\s*%'  ], ],
    'Markdown'           : [   [ call_regexp_common  , 'HTML'   ], ],
    'Modula3'            : [   [ call_regexp_common  , 'Pascal' ], ],
        # Modula 3 comments are (* ... *) so applying the Pascal filter
        # which also treats { ... } as a comment is not really correct.
    'NVIDIA CUDA'        : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Objective C'        : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Objective C++'      : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Ocaml'              : [
                                [ call_regexp_common  , 'Pascal' ],
                            ],
    'PHP/Pascal'               : [ [ die ,          ], ], # never called
    'MATLAB/Objective C/MUMPS' : [ [ die ,          ], ], # never called
    'MUMPS'              : [   [ remove_matches      , '^\s*;'  ], ],
    'Octave'             : [   [ remove_matches      , '^\s*#'  ], ],
    'Oracle Forms'       : [   [ call_regexp_common  , 'C'      ], ],
    'Oracle Reports'     : [   [ call_regexp_common  , 'C'      ], ],
    'Pascal'             : [
                                [ call_regexp_common  , 'Pascal' ],
                                [ remove_matches      , '^\s*//' ],
                            ],
    'Patran Command Language': [
                                [ remove_matches      , '^\s*#'   ],
                                [ remove_matches      , '^\s*\$#' ],
                                [ call_regexp_common  , 'C'       ],
                            ],
    'Perl'               : [    [ remove_below        , '^__(END|DATA)__'],
                                [ remove_matches      , '^\s*#'  ],
                                [ remove_below_above  , '^=head1', '^=cut'  ],
                            ],
    'Python'             : [
                                [ remove_matches      , '^\s*#'  ],
                                [ docstring_to_C                 ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'PHP'                : [
                                [ remove_matches      , '^\s*#'  ],
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Rexx'               : [   [ call_regexp_common  , 'C'      ], ],
    'Ruby'               : [   [ remove_matches      , '^\s*#'  ], ],
    'Ruby HTML'          : [   [ call_regexp_common  , 'HTML'   ], ],
    'reStructuredText'   : [   [ remove_matches      , '^\s*\.\.'  ], ],
    'Scala'              : [
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'SKILL'              : [
                                [ call_regexp_common  , 'C'      ],
                                [ remove_matches      , '^\s*;'  ],
                            ],
    'SKILL++'            : [
                                [ call_regexp_common  , 'C'      ],
                                [ remove_matches      , '^\s*;'  ],
                            ],
    'SQL'                : [
                                [ call_regexp_common  , 'C'      ],
                                [ remove_matches      , '^\s*--' ],
                            ],
    'SQL Stored Procedure': [
                                [ call_regexp_common  , 'C'      ],
                                [ remove_matches      , '^\s*--' ],
                            ],
    'SQL Data'           : [
                                [ call_regexp_common  , 'C'      ],
                                [ remove_matches      , '^\s*--' ],
                            ],
    'sed'                : [   [ remove_matches      , '^\s*#'  ], ],
    'Smarty'             : [
                                [ smarty_to_C                    ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'System Verilog'     : [
                                [ remove_matches      , '^\s*//' ], # C99
                                [ call_regexp_common  , 'C'      ],
                            ],
    'Bourne Again Shell' : [   [ remove_matches      , '^\s*#'  ], ],
    'Bourne Shell'       : [   [ remove_matches      , '^\s*#'  ], ],
    'm4'                 : [   [ remove_matches      , '^dnl '  ], ],
    'C Shell'            : [   [ remove_matches      , '^\s*#'  ], ],
    'Kermit'             : [
                                [ remove_matches      , '^\s*#'  ],
                                [ remove_matches      , '^\s*;'  ],
                            ],
    'Korn Shell'         : [   [ remove_matches      , '^\s*#'  ], ],
    'Tcl/Tk'             : [   [ remove_matches      , '^\s*#'  ], ],
    'Teamcenter def'     : [   [ remove_matches      , '^\s*#'  ], ],
    'Teamcenter met'     : [   [ call_regexp_common  , 'C'      ], ],
    'Teamcenter mth'     : [   [ remove_matches      , '^\s*#'  ], ],
    'Softbridge Basic'   : [   [ remove_above        , '^\s*Attribute\s+VB_Name\s+=' ],
                                [ remove_matches      , '^\s*Attribute\s+'],
                                [ remove_matches      , '^\s*\47'], ],  # \47 = '
    # http://www.altium.com/files/learningguides/TR0114%20VHDL%20Language%20Reference.pdf
    'Verilog'            : [
                                [ remove_matches      , '^\s*//' ], # C99
                                [ call_regexp_common  , 'C'      ],
                            ],
    'VHDL'               : [
                                [ remove_matches      , '^\s*--' ],
                                [ remove_matches      , '^\s*//' ],
                                [ call_regexp_common  , 'C'      ],
                            ],
    'vim script'         : [   [ remove_matches      , '^\s*"'  ], ],
    'Visual Basic'       : [   [ remove_above        , '^\s*Attribute\s+VB_Name\s+=' ],
                                [ remove_matches      , '^\s*Attribute\s+'],
                                [ remove_matches      , '^\s*\47'], ],  # \47 = '
    'yacc'               : [   [ call_regexp_common  , 'C'      ], ],
    'YAML'               : [   [ remove_matches      , '^\s*#'  ], ],
    'lex'                : [   [ call_regexp_common  , 'C'      ], ],
    'XAML'               : [   [ call_regexp_common  , 'HTML'   ], ],
    'MXML'               : [   [ call_regexp_common  , 'HTML'   ], ],
    'XML'                : [   [ call_regexp_common  , 'HTML'   ], ],
    'XSD'                : [   [ call_regexp_common  , 'HTML'   ], ],
    'XSLT'               : [   [ call_regexp_common  , 'HTML'   ], ],
    'NAnt scripts'       : [   [ call_regexp_common  , 'HTML'   ], ],
    'MSBuild scripts'    : [   [ call_regexp_common  , 'HTML'   ], ],
}

RE_COMMENT = {
    'HTML'   : '<!--[\s\S]*?--\s*>',
    'C'      : '/\*[\s\S]*?\*/',
    'Pascal' : '(\{[\s\S]*?\})|(\(\*[\s\S]*?\*\))',
}

cmdtable = {
    "cloc":
        (cloc,
         [('r', 'rev', [],
           _('count lines of source code for the specified revision'), _('REV')),
          ('f', 'file', None, _('report results for every source file encountered')),
          ('i', 'ignore', [],
           _('Ignore files and/or directories specified'), _('PATTERN')),
          ] + commands.walkopts,
         _("hg cloc [-r REV]")),
}
